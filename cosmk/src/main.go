package main

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/alecthomas/kong"
	"github.com/posener/complete"
	"github.com/willabides/kongplete"
)

type Context struct {
}

type InstrumentationFeaturesCmd struct {
	Feature string `help:"Test if given instrumentation feature is enabled."`
}

func (r *InstrumentationFeaturesCmd) Run(ctx *Context) error {
	doInstrumentationFeatures()
	return nil
}

type DocCmd struct {
	Build bool `cmd:"" help:"Build the documentation."`
	Open  bool `cmd:"" help:"Open the documentation in the default browser, building it if necessary."`
	Clean bool `cmd:"" help:"Remove documentation build folder."`
}

func (r *DocCmd) Run(ctx *Context) error {
	if r.Build {
		doBuildDoc()
	} else if r.Open {
		doOpenDoc()
	} else if r.Clean {
		doCleanDoc()
	}
	return nil
}

type BootstrapCmd struct {
	Recipe string `required:"" arg:"" help:"recipe to use, must be one of \"${sdks_list}\""`
}

func (r *BootstrapCmd) Run(ctx *Context) error {
	findContainerRuntime()
	s := parseSdkConfig(r.Recipe)
	err := s.bootstrap()
	if err != nil {
		Error.Fatalf("Error: %s", err)
	}
	return nil
}

type ContainerCmd struct {
	Recipe string `required:"" enum:"${sdks_list}" arg:"" help:"Sdk to use, must be onf of \"${sdks_list}\""`
}

func (r *ContainerCmd) Run(ctx *Context) error {
	findContainerRuntime()
	s := parseSdkConfig(r.Recipe)
	fmt.Fprintf(os.Stdout, "%s/%s:%s", rootConfig.Product.Name, s.Name, s.Tag)
	return nil
}

type RunCmd struct {
	Recipe  string   `required:"" enum:"${list_all}" arg:"" help:"SDK or recipe to use, must be one of \"${list_all}\""`
	Command []string `optional:"" arg:"" help:"Command with arguments to run inside the SDK"`
}

func (r *RunCmd) Run(ctx *Context) error {
	findContainerRuntime()
	command := []string{"bash"}

	if r.Command != nil && len(r.Command) > 0 {
		command = r.Command
	}
	for _, s := range listSdks() {
		if s == r.Recipe {
			sdk := parseSdkConfig(r.Recipe)
			sdk.run(command)
			return nil
		}
	}

	c := parseRecipeConfig(r.Recipe)
	c.run(command)
	return nil
}

type BuildCmd struct {
	Recipe string `required:"" enum:"${recipes_list}" arg:"" help:"recipe to use, must be one of \"${recipes_list}\""`
}

func (r *BuildCmd) Run(ctx *Context) error {
	findContainerRuntime()
	Recipe := parseRecipeConfig(r.Recipe)
	Recipe.action("build")
	return nil
}

type ImageCmd struct {
	Recipe string `required:"" enum:"${recipes_list}" arg:"" help:"recipe to use, must be one of \"${recipes_list}\""`
}

func (r *ImageCmd) Run(ctx *Context) error {
	findContainerRuntime()
	Recipe := parseRecipeConfig(r.Recipe)
	Recipe.action("image")
	return nil
}

type ConfigureCmd struct {
	Recipe string `required:"" enum:"${recipes_list}" arg:"" help:"recipe to use, must be one of \"${recipes_list}\""`
}

func (r *ConfigureCmd) Run(ctx *Context) error {
	findContainerRuntime()
	Recipe := parseRecipeConfig(r.Recipe)
	Recipe.action("configure")
	return nil
}

type BundleCmd struct {
	Recipe string `required:"" enum:"${recipes_list}" arg:"" help:"recipe to use, must be one of \"${recipes_list}\""`
}

func (r *BundleCmd) Run(ctx *Context) error {
	findContainerRuntime()
	Recipe := parseRecipeConfig(r.Recipe)
	Recipe.action("bundle")
	return nil
}

type TestCmd struct {
	Setup bool `help:"Setup the testbed environment (build the vagrant boxes and call 'vagrant up'."`
	Qemu  bool `help:"Create a QEMU image for testing and start a VM in the virtual testbed."`
	Start bool `help:"Start a VM in the virtual testbed."`
}

func (r *TestCmd) Run(ctx *Context) error {
	if r.Setup {
		doTestbedSetup()
	} else if r.Qemu {
		doTestbedQemu()
	} else if r.Start {
		doTestbedRun()
	}
	return nil
}

type RepoRootPathCmd struct {
}

func (r *RepoRootPathCmd) Run(ctx *Context) error {
	fmt.Fprintf(os.Stdout, repoRootPath)
	return nil
}

type ProductNameCmd struct {
}

func (r *ProductNameCmd) Run(ctx *Context) error {
	fmt.Fprintf(os.Stdout, parseProductConfig().Short_name)
	return nil
}

type ProductVersionCmd struct {
}

func (r *ProductVersionCmd) Run(ctx *Context) error {
	fmt.Fprintf(os.Stdout, parseProductConfig().Version)
	return nil
}

type CiRegistryCmd struct {
}

func (r *CiRegistryCmd) Run(ctx *Context) error {
	fmt.Fprintf(os.Stdout, rootConfig.Ci.Registry)
	return nil
}

type CiArtifactsCmd struct {
}

func (r *CiArtifactsCmd) Run(ctx *Context) error {
	fmt.Fprintf(os.Stdout, rootConfig.Ci.Artifacts)
	return nil
}

type CacheCmd struct {
}

func (r *CacheCmd) Run(ctx *Context) error {
	doCache()
	return nil
}

type AllCmd struct {
}

func (r *AllCmd) Run(ctx *Context) error {
	findContainerRuntime()
	err := parseProductConfig().do()
	if err != nil {
		Error.Fatalf("Error: %s", err)
	}
	return nil
}

type ReconfCmd struct {
}

func (r *ReconfCmd) Run(ctx *Context) error {
	findContainerRuntime()
	err := parseProductConfig().reconfigure()
	if err != nil {
		Error.Fatalf("Error: %s", err)
	}
	return nil
}

type VersionCmd struct {
}

func (r *VersionCmd) Run(ctx *Context) error {
	fmt.Fprintf(os.Stdout, version)
	return nil
}

var cosmk struct {
	Debug                   bool                       `help:"Enable debug output."`
	InstrumentationFeatures InstrumentationFeaturesCmd `cmd:"" help:"List of enabled instrumentation features set in config.toml."`
	Version                 VersionCmd                 `cmd:"" help:"Prints cosmk version."`
	RepoRootPath            RepoRootPathCmd            `cmd:"" help:"Output on stdout the repo root absolute path."`
	ProductName             ProductNameCmd             `cmd:"" help:"Output on stdout the product name set in config.toml."`
	ProductVersion          ProductVersionCmd          `cmd:"" help:"Output on stdout the product version set in config.toml."`
	CiRegistry              CiRegistryCmd              `cmd:"" help:"Output on stdout the registry configured in the CI section in config.toml."`
	CiArtifacts             CiArtifactsCmd             `cmd:"" help:"Output on stdout the artifacts URL configured in the CI section in config.toml."`
	Cache                   CacheCmd                   `cmd:"" help:"Download pre-built binary packages from the CI."`
	Doc                     DocCmd                     `cmd:"" help:"Build, Open or Clean documentation of the project."`
	All                     AllCmd                     `cmd:"" help:"Run all steps required to build a product."`
	Reconf                  ReconfCmd                  `cmd:"" help:"Build a product but skip 'build' & 'image' steps (iterative rebuild/reconfiguration.)"`
	Bootstrap               BootstrapCmd               `cmd:"" help:"Bootstrap a SDK recipe."`
	Container               ContainerCmd               `cmd:"" help:"Output on stdout the container name and tag for the given Sdk recipe."`
	Run                     RunCmd                     `cmd:"" help:"Start a shell in the SDK set for a recipe."`
	Build                   BuildCmd                   `cmd:"" help:"Build from source the rootfs of a given recipe."`
	Image                   ImageCmd                   `cmd:"" help:"Build the rootfs of a given recipe from the cache produced during the \"build\" action step."`
	Configure               ConfigureCmd               `cmd:"" help:"Apply configuration scripts for a given recipe."`
	Bundle                  BundleCmd                  `cmd:"" help:"Bundle a recipe."`
	Test                    TestCmd                    `cmd:"" help:"Setup, Create or Run the project virtual testbed."`
}

func main() {
	// Find the project root directory
	repoRootPath = findRepoRootPath()

	// Initial version check to make sure that we are running the version currently available in the repository
	if version == "" {
		log.Fatal("cosmk was not build with version information. Please see the README.")
	}
	versionCheck(version)

	// Parse config.toml to find the selected product
	parseConfig()

	// Setup and parse command line
	parser := kong.Must(&cosmk,
		kong.Name("cosmk"),
		kong.Vars{
			"sdks_list":    strings.Join(listSdks(), ", "),
			"list_all":     strings.Join(listAll(), ", "),
			"recipes_list": strings.Join(listRecipes(), ", "),
		},
		kong.UsageOnError(),
	)

	kongplete.Complete(parser,
		kongplete.WithPredictor("file", complete.PredictFiles("*")),
	)

	ctx, err := parser.Parse(os.Args[1:])
	parser.FatalIfErrorf(err)

	// Setup logging now that we know if we are running in debug mode or not
	initLogging(&cosmk.Debug)

	err = ctx.Run(&Context{})
	ctx.FatalIfErrorf(err)
}
