module cosmk

go 1.13

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/alecthomas/kong v0.2.17
	github.com/posener/complete v1.2.3
	github.com/willabides/kongplete v0.2.0
)
